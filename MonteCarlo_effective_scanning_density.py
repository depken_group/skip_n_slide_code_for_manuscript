import numpy as np
import click
import tqdm

'''
Use Monte Carlo simulation (Gillespie algorithm) to simulate Pcheck(rho)

* The simulation simulates random walkers: either they move to there neighbors or unbind 
* Before making a move, the walker can 'interrogate site n' with fixed probability 

The code returns:
* The fraction of visited sites that got interrogated for every run
* The span of the lateral excursion for every run
* both are written to output file 


Parameters: 
* rho : probability to interrogate a site 
* u : unbinding rate per site 
* k : stepping rate (set to 1) 
* number of runs Monte Carlo simulation 
* file name for output
'''

###########################################
##  main Monte Carlo simulation         ###
###########################################
@click.command()
@click.option('--rho_sns', type=float, default=0.3, help='scanning density during one sNs-cycle')
@click.option('--u',type=float, default=0.1, help='unbinding rate')
@click.option('--k',type=float, default=1.0, help='rate of a skip or slide')
@click.option('--number_runs', type=int, default=1, help='number of repetitions')
@click.option('--filename', type=str, default='MC_sim_rhoRND.txt',
              help='store simulated spans of excursions and effective scanning densities to TXT-file')
def random_walker_MC(rho_sns, u, k, number_runs, filename):
    '''
    Monte Carlo simulation to determine the effective scanning density (rho_RND)
    at a fixed unbinding rate (u), rather than a fixed unbinding position (or fixed span of lateral excursion
    l_RND).
    Every site, representing one sNs cycle, will get interrogated with a probability rhoSNS.
    rho_RND is estimated as the number of interrogated sites between landing and unbinding positions.
    '''

    # --- open output file ----
    output = open(filename, 'w')

    # --- initial position ----
    n0 = 0

    # --- main loop Monte Carlo simulation ---
    for run in tqdm.tqdm(range(number_runs)):

        # --- restart simulation ---
        n = n0
        n_max = n0
        n_min = n0
        checked_sites = []
        protein_is_bound = True

        # -- start random walker ---
        while protein_is_bound:
            # --- step 1: Do I interrogate current site? ---
            check_current_site(rho_sns, n, checked_sites)

            # --- step 2: make a move ----
            n, protein_is_bound = make_move(u,k, n, protein_is_bound)

            # -- step 3: update min/max visited sites ---
            n_max = np.maximum(n_max, n)
            n_min = np.minimum(n_min, n)


        # --- determine the span of lateral excursion and number of interrogated sites along the way---
        N_min = np.minimum(n0,n)
        N_max = np.maximum(n0,n)
        l_RND = N_max-N_min + 1
        checked_sites = np.array(checked_sites)
        within_lRND = np.where((checked_sites <= N_max) & (checked_sites >= N_min))[0]
        rho_RND = len(np.unique(checked_sites[within_lRND])) / float(l_RND)

        # --- write to file ---
        output.write(str(rho_RND) + '\t' + str(l_RND) +'\n'   )

    # --- close output file ---
    output.close()

    return



######################################################
##                 functions                       ###
######################################################
def check_current_site(rho, n , list_of_checked_sites):
    U = np.random.uniform()
    if U < rho:
        list_of_checked_sites.append(n)
    return

def make_move(u, k , n, protein_is_bound):
    p_left = k/(2*k + u)
    p_right= k/(2*k + u)
    p_ub =  u/(2*k+u)
    split_prob = [p_left,p_right,p_ub]


    tower_sampling = np.cumsum(split_prob)
    U = np.random.uniform()
    if U < tower_sampling[0]:
            n -= 1
    elif U < tower_sampling[1]:
            n += 1
    else:
        protein_is_bound = False
    return n, protein_is_bound


###########################################
##  Cluster: Command line main function ###
###########################################
if __name__ == "__main__":
    random_walker_MC()