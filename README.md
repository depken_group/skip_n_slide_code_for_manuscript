# Getting started 
The code in this repository is used for the work presented in our manuscript _"Skipping and sliding to optimize target search on protein-bound DNA and RNA"_ ([preprint](https://www.biorxiv.org/content/10.1101/2020.06.04.133629v1.full)).
To start, obtain the required Python 3 packages using
```
pip install -r requirements.txt
```

# Calculate shuttling times in tandem target assay based on sNs search
The code `MFPT_tandem_trap_assay.py` evaluates the mean first passage time of a initially trapped protein 
reaching the opposite trap as a function of the number of nucleotides separating the two traps (__Figures 1 and 2 of the manuscript__).
```
Usage: MFPT_tandem_trap_assay.py [OPTIONS]

  loop over distances between targets and determine MFPT

  1) Choose the model parameters 2) for every distance     A. build the
  matrix / Master Equation     B. Solve for MFPT

  3) Will return the results as output. Optional: store the results into a
  TXT file and/orplot the result

Options:
  --l_slide FLOAT        sliding length in nt
  --mu_skip FLOAT        average skipping distance in nt
  --sigma_skip FLOAT     spread (standard deviation) of skipping distance in
                         nt
  --tau_trap FLOAT       escape time from trap in seconds
  --tau_ns FLOAT         escape time non-specific site in seconds
  --flank_left INTEGER   # nt flanking left-most trap
  --flank_right INTEGER  # nt flanking right-most trap
  --save_file BOOLEAN    Store the MFPT curve into a TXT-file?
  --filename TEXT        if save_output==TRUE: Provide a file name for
                         resulting MFPT values
  --plot_result BOOLEAN  Display the MFPT curve?
  --help                 Show this message and exit.
```
Example: `python MFPT_tandem_trap_assay.py --l_slide=10. --mu_skip=30. --sigma_skip=0.001`.  
Default argument values (calling `python MFPT_tandem_trap_assay.py`) correspond to estimated values for Argonaute proteins (__see Figure 2 of manuscript__). 

# Simulating effective scanning density (genome scale) based on scanning density in sNs cycles 
To justify our approximations of the effective scanning density, 
```math
\rho_{\rm{RND}}
```
, **Equation S16 of the manuscript**, we turned to Monte Carlo simulations (__Figure S4__, details given in Supplement of manuscript).
See `MonteCarlo_effective_scanning_density.py`, 
```
Usage: MonteCarlo_effective_scanning_density.py [OPTIONS]

  Monte Carlo simulation to determine the effective scanning density
  (rho_RND) at a fixed unbinding rate (u), rather than a fixed unbinding
  position (or fixed span of lateral excursion l_RND). Every site,
  representing one sNs cycle, will get interrogated with a probability
  rhoSNS. rho_RND is estimated as the number of interrogated sites between
  landing and unbinding positions.

Options:
  --rho_sns FLOAT        scanning density during one sNs-cycle
  --u FLOAT              unbinding rate
  --k FLOAT              rate of a skip or slide
  --number_runs INTEGER  number of repetitions
  --filename TEXT        store simulated spans of excursions and effective
                         scanning densities to TXT-file
  --help                 Show this message and exit.
```
Example: `python MonteCarlo_effective_scanning_density.py --rho_sns=0.3 --u=0.01, --k=1.0, --number_runs=1000, --filename=MC_results.txt`

