import numpy as np
from scipy import linalg
from scipy import special
import matplotlib.pylab as plt

import click
import tqdm
'''
calculate MFPT vs distance between traps on tandem target construct using the Master Equation

By choosing a sliding length (lslide) and skipping length (lskip^2 = mu^2 + sigma^2), we construct the (effective) rate of 
moving from site_i to site_j. From this we get the Master Equation. 
The inverse of the appropriate transfer matrix is used to determine the MFPT: "Starting at the first trap, what is the MFPT 
for arriving at the opposite trap"? 
'''




######################################################
##  main function: returns/saves MFPT vs distance  ###
######################################################
@click.command()
@click.option('--l_slide', type=float, default=10,help='sliding length in nt')
@click.option('--mu_skip', type=float, default=30,help='average skipping distance in nt')
@click.option('--sigma_skip',type=float, default=0.001,
              help='spread (standard deviation) of skipping distance in nt')
@click.option('--tau_trap', type=float,default=1.0, help='escape time from trap in seconds')
@click.option('--tau_ns', type=float, default=10**(-4), help='escape time non-specific site in seconds')
@click.option('--flank_left', type=int,default=20 ,help='# nt flanking left-most trap')
@click.option('--flank_right', type=int,default=30 ,help='# nt flanking right-most trap')
@click.option('--save_file', default=True,type=bool, help='Store the MFPT curve into a TXT-file?')
@click.option('--filename' , default='MFPT_test.txt', help='if save_output==TRUE: Provide a file name for resulting MFPT values')
@click.option('--plot_result', default=True,type=bool, help='Display the MFPT curve?')
def MFPT_curve(l_slide, mu_skip, sigma_skip,tau_trap, tau_ns, flank_left, flank_right,
               filename, save_file, plot_result):
    '''
    loop over distances between targets and determine MFPT

    1) Choose the model parameters
    2) for every distance
        A. build the matrix / Master Equation
        B. Solve for MFPT

    3) Will return the results as output.
    Optional: store the results into a TXT file and/orplot the result
    '''
    distances = list(range(1,201))
    T = []
    for l in tqdm.tqdm(distances):
        parameters = ModelParameters(l, l_slide, mu_skip, sigma_skip, tau_trap, tau_ns, flank_left, flank_right)
        MEqn = build_matrix(parameters)
        T.append(MeanFirstPassageTime(MEqn, parameters))
    if save_file:
        np.savetxt(filename,T)

    if plot_result:
        plt.figure()
        plt.plot(distances, T)
        plt.xlabel('trap separation ' + r'$d_{\rm{trap}}$ ' + '(nt)')
        plt.ylabel('shuttling time ' + r'$T_{\rm{shuttle}}$ ' + '(s)')
        plt.show()
    return T



def main(argv):
    # --- sweep selected parameters using cluster ----
    lslide = float(argv[1])
    mu_skip = float(argv[2])
    sigma_skip = float(argv[3])
    file_name = argv[4]

    # --- others are kept fixed --> run simulation ------
    distances = [i for i in range(1,201)]
    MFPT_curve(distances, lslide, mu_skip, sigma_skip,
               tau_trap=1.0,
               tau_ns=10**(-4),
               flank_left=0,
               flank_right=0,
               file_out=file_name,
               save_file=True)

    print('completed simulation....')
    print('lslide: ', lslide)
    print('mu_skip: ', mu_skip)
    print('sigma_skip: ',sigma_skip)
    return







######################################################
##          global parameters                      ###
######################################################
class ModelParameters():
    def __init__(self, l, lslide, mu_skip, sigma_skip, tau_trap=1.0, tau_ns=10**(-4), flank_left=0, flank_right=0):
        # model parameters
        self.tau_T = tau_trap
        self.tau_ns = tau_ns
        self.mu_skip = mu_skip
        self.sigma_skip = sigma_skip
        self.lskip = np.sqrt(mu_skip ** 2 + sigma_skip ** 2)
        self.lslide = lslide

        # system parameters:
        self.distance = l
        self.flank_left = flank_left
        self.flank_right = flank_right
        self.trap1 = flank_left
        self.trap2 = self.trap1 + l
        self.RNAlength = self.flank_left + self.distance + self.flank_right + 1




######################################################
##                 functions                       ###
######################################################
def MeanFirstPassageTime(M, parameters):
    '''
    Determing MFPT using system of equations 'M'
    :param M:
    :param parameters:
    :return:
    '''

    # --- unpack parameters ----
    L = parameters.RNAlength
    trap1 = parameters.trap1
    trap2 = parameters.trap2

    # --- initial condition ---
    P0 = np.zeros(L)
    P0[trap1] = 1.0
    P0 = np.delete(P0, trap2)

    # -- calculate MFPT ---
    Minv = np.linalg.inv(M)
    vec = np.ones(len(M))
    MFPT = vec.dot(Minv.dot(P0))  # <vec| M^{-1} | P0>
    return MFPT


def build_matrix(parameters):
    '''
    Builds system of Master Equations
    '''
    # --- unpack parameters ---
    L = parameters.RNAlength
    ls = parameters.lslide
    mu_skip = parameters.mu_skip
    sigma_skip = parameters.sigma_skip
    trap1 = parameters.trap1
    trap2 = parameters.trap2
    tau_T = parameters.tau_T
    tau_ns= parameters.tau_ns

    R = np.zeros([L, L])
    for i in range(len(R)):
        for j in range(len(R)):
            if i == j:
                continue
            else:
                R[i, j] = calc_rate(j, i, mu_skip, sigma_skip, ls, tau_T,tau_ns, sites_trap=[trap1, trap2])

    # Fix the diagonal elements:
    R = R - np.diag(R)
    for n in range(L):
        R[n, n] = -np.sum(R[:, n])

    # Apply absorbing state:
    R[:, trap2] = 0

    # fix overal minus sign:
    R *= -1

    # Reduced system of equations ommiting the absorbing state:
    M = np.delete(np.delete(R, trap2, axis=0), trap2, axis=1)
    return M



def calc_rate(site_i,site_j,mu_skip, sigma_skip, lslide, tau_T, tau_ns, sites_trap):
    '''
    rate from site_i to site_j
    '''
    kappa = total_rate(site_i,sites_trap,tau_T,tau_ns)
    return kappa * Weight(site_i,site_j,mu_skip, sigma_skip, lslide)


def total_rate(site_i,sites_trap, tau_T, tau_ns):
    if (site_i == sites_trap[0]) or (site_i == sites_trap[1]):
        kappa = tau_T**(-1)
    else:
        kappa = tau_ns**(-1)
    return kappa

def Weight(site_i,site_j,mu_skip, sigma_skip, lslide):
    ds = np.abs(site_i - site_j)
    pslide = lslide**2/(1 + lslide**2)
    return pslide * kronecker_delta(ds,1) + (1-pslide)*skips(ds, mu_skip, sigma_skip)


def skips(x, mu, sigma):
    # --- integral of Gaussian from (x-0.5)-->(x+0.5)
    rhoB = 0.5 * (1 + special.erf((x + 0.5 - mu) / (sigma * np.sqrt(2))))
    rhoA = 0.5 * (1 + special.erf((x - 0.5 - mu) / (sigma * np.sqrt(2))))
    rho_plus = rhoB - rhoA

    # --- integral using mu-->-mu
    rhoB = 0.5 * (1 + special.erf((x + 0.5 + mu) / (sigma * np.sqrt(2))))
    rhoA = 0.5 * (1 + special.erf((x - 0.5 + mu) / (sigma * np.sqrt(2))))
    rho_minus = rhoB - rhoA

    rho = rho_plus + rho_minus
    return rho



def kronecker_delta(n,m):
    if n == m:
        delta = 1.0
    else:
        delta = 0.0
    return delta





###########################################
##  Command line main function          ###
###########################################
if __name__ == "__main__":
    MFPT_curve()